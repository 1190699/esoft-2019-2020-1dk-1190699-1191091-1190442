# UC9 - Efetuar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve

Um freelancer apenas pode candidatar-se a um anúncio     para     o  qual     é     elegível, isto    é, quando lhe é 
reconhecido    possuir    o    grau    de    proficiência    mínimo    exigido    a todas as competências técnicas
obrigatórias para a tarefa em causa. Ao efetuar uma candidatura, este deve obrigatoriamente indicar o valor pretendido 
pela realização da tarefa bem com o número de dias necessários à sua realização após atribuição da mesma. 
Opcionalmente, pode incluir um texto de apresentação e/ou motivação.

### SSD
![UC9_SSD.svg](UC9_SSD.svg)


### Formato Completo

#### Ator principal

* Freelancer

#### Partes interessadas e seus interesses
 
(identificar as partes interessadas e seus interesses)

* **Administrativo:** pretende especificar as competências técnicas requeridas para a realização de tarefas.
* **Freelancer:** pretende conhecer as competências técnicas que podem ser requeridas para a realização de tarefas.
* **Organização:** pretende contratar pessoas com as competências técnicas requeridas para a realização de tarefas.
* **T4J:** pretende que as competências técnicas estejam descritas com rigor/detalhe.

#### Pré-condições

* n/a

#### Pós-condições

* n/a

#### Cenário de sucesso principal (ou fluxo básico)

1. O ~~administrativo~~ **Freelancer** inicia a ~~especificação~~ **Candidatura** de ~~uma competência técnica~~ **um anúncio a qual foi eleito**.
2. O sistema solicita os dados necessários (i.e. ~~código único, descrição breve e detalhada~~ **valor pretendido,número de dias,texto de apresentação(opcional)**).
3. O ~~administrativo~~ **Freelancer** introduz os dados solicitados.
4. O sistema mostra a lista de ~~áreas de atividade~~ **anúncios** e pede ao ~~administrativo~~ **Freelancer** para selecionar uma.
5. O ~~administrador~~ **Freelancer** seleciona ~~uma área de atividade~~ **um anúncio**.
6. O sistema valida e apresenta os dados ao ~~administrativo~~ **Freelancer**, pedindo que os confirme.
7. O ~~administrativo~~ **Freelancer** confirma os dados.
8. O sistema regista os dados e informa o ~~administrativo~~ **Freelancer** do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O ~~administrativo~~ **Freelancer** solicita o cancelamento da ~~especificação da competência técnica~~ **candidatura de um anúncio**.  
> O caso de uso termina.

4a. Não existem ~~áreas de atividade definidas~~ **anúncios definidos** no sistema.  
>1. O sistema informa o ~~administrativo~~ **Freelancer** de tal facto.  
>2. O sistema permite a criação de uma nova ~~área de atividade(UC2)~~ **tarefa(UC9)**.  
> 2a. O ~~administrativo~~ **Freelancer** não ~~cria uma área de atividade~~ **efetua a candidatura**. O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3).  
> 2a. O ~~administrativo~~ **Freelancer** não introduz os dados em falta. O caso de uso termina.

6b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o ~~administrativo~~ **Freelancer** para o facto.
>	2. O sistema permite a sua alteração (passo 3).  
> 2a. O ~~administrativo~~ **Freelancer** não altera os dados. O caso de uso termina.

6c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o ~~administrativo~~ **Freelancer** para o facto.
> 2. O sistema permite a sua alteração (passo 3)..  
> 2a. O ~~administrativo~~ **Freelancer** não altera os dados. O caso de uso termina. 

#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência
(descrever com que frequência este UC é realizado)

\-

#### Questões em aberto

(lista de questões em aberto, i.e. sem uma resposta conhecida.)

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC9_MD.svg](UC9_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Freelancer inicia a candidatura.  		 |...interage com o utilizador?							 |EfetuarCandidaturaUI             |Pure Fabrication: não se justifica atribuir esta responsabilidade a nenhuma classe existente                              |
|        		 |...coordena o UC?							 |EfetuarCandidaturaController             |Controller                              |
|            	 |...cria instância de candidatura?							 |Anuncio             |Creator (Regra 1): no MD o Anuncio tem Candidatura                              |
|        		 |...conhece o utilizador/freelancer a usar o sistema?							 |SessaoUtilizador             |IE: documentação do componente de gestão de utilizadores.                              |
| 2. O sistema apresenta a lista de anúncios cujo período de candidaturas está em vigor e que são elegíveis para o Freelancer.		 |...separa os anuncios (com tarefas elegíveis para o Freelancer)?							 |Plataforma             |IE: A Plataforma tem o RegistoAnuncio.                              |
|   		 |...conhece todos os anúncios da plataforma?							 |RegistoAnuncio             |HC + LC: o RegistoAnuncio conhece todos os anúncios.                              |
| 	 |...conhece a tarefa?							 |Anuncio             |IE: conhece a categoria a tarefa                              |
| 	 |							 |      Tarefa       |     IE: tem categoria tarefa                         |
| 	 |							 |    Categoria         |IE: competencias tecnicas podem ser obrigatórias ou desejáveis                              |
| 	 |							 |     CompetenciaTecnica        |IE: tem um grau de proficiencia                              |
| 3. O Freelancer seleciona o anúncio ao qual se pretende candidatar.  		 |						 |             |              |
| 4. O sistem solicita os dados necessários para efetuar a candidatura (i.e valor pretendido pela realização da tarefa, número de dias  necessários para completar a mesma).  		 |							 |             |                              |
| 5. O Freelancer introduz os dados solicitados.  		 |...guarda os dados introduzidos?							 |Candidatura             |Information Expert (IE) - instância criada no passo 1.                              |              
| 6. O sistema valida e apresenta os dados  ao Freelancer, pedindo que os confirme.  		 |...valida os dados da Candidatura (validação local)?							 |Candidatura             |IE: A candidatura possui os seus próprios dados.                              |              
|   		 |...valida os dados da candidatura (validação global)?							 |ListaCandidatura             |HC + LC: a ListaCandidatura conhece todas as candidaturas do respetivo Anuncio                             |              
| 7. o Freelancer confirma.   		 |							 |             |                              |              
| 8. O sistema regista a candidatura do Freelancer e informa-o do sucesso da operação.  		 |...guarda a Candidatura criada?							 |ListaCandidatura             |HC + LC: a ListaCandidatura guarda todas as candidaturas do respetivo Anuncio                                        |              


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Anuncio
 * Plataforma
 * Tarefa
 * Categoria
 
 
 
 
 
Outras classes de software (i.e. Pure Fabrication) identificadas:  

* EfetuarCandidaturaUI
* EfetuarCandidaturaController
* CompetenciaTecnica
* Candidatura
* ListaCandidatura
* SessaoUtilizador
* RegistoAnuncio


###	Diagrama de Sequência

![UC9_SD.svg](UC9_SD.svg)

###	Diagramas de Sequência Secundários

###GetFreelancerByEmail
![UC9_SD_GetFreelancerByEmail.svg](UC9_SD_GetFreelancerByEmail.svg)

###GetListaAnunciosElegiveis
![UC9_SD_GetListaAnunciosElegiveis.svg](UC9_SD_GetListaAnunciosElegiveis.svg)


###	Diagrama de Classes

![UC9_CD.svg](UC9_CD.svg)
