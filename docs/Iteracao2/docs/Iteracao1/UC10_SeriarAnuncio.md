# UC 10 - Seriar Anúncio

## 1. Engenharia de Requisitos

### Formato Breve

O Colaborador inicia o processo de seriação de candidaturas ao anúncio. O sistema apresenta a lista de candidaturas ao Anúncio. O Colaborador procede à classificação das candidaturas. O sistema solicita os dados relativos ao processo de seriação (i.e data, hora e participantes). O Colaborador introduz os dado pedidos. O sistema apresenta os dados e solicita confirmação. O Colaborador confirma. O sistema regista os dados e informa o Colaborador do sucesso da operação.


### SSD
![UC10_SSD](UC10_SSD.png)


### Formato Completo

#### Ator principal

Colaborador

#### Partes interessadas e seus interesses


* **Colaborador:** pretende executar a seriação de candidaturas a um anúncio de modo a proceder à posterior escolha de um _Freelancer_.
* **_Freelancer_:** pretende saber a sua classificação relativamente a anúncio.

#### Pré-condições

n/a

#### Pós-condições
Lista ordenada das colocações de _Freelancer_ para um certo anúncio.

#### Cenário de sucesso principal (ou fluxo básico)


1. O Colaborador inicia a seriação de candidaturas ao anúncio.
2. O sistema apresenta a lista de anúncios possíveis de seriação.
3. O Colaborador seleciona o anúncio que tenciona seriar.
4. O sistema apresenta a lista de candidaturas referente ao anúncio.
5. O Colaborador procede à classificação das candidaturas.
6. O sistema apresenta uma lista de Colaboradores.
7. O Colaborador seleciona os Colaboradores intervenientes na seriação.
8. O sistema pede para ser inserida a data e hora da seriação.
9. O Colaborador introduz os dados.
10. O sistema valida e apresenta os dados, pedindo que os confirme.
11. O Colaborador confirma.
12. O sistema regista os dados e informa o Colaborador do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O Colaborador solicita o cancelamento da registo.

> O caso de uso termina.

2a. Não existem anúncios possíveis de seriação.
> 1. O caso de uso termina.

3a. Dados relativos ao anúncio não selecionados.
>	1. O sistema informa o sucedido.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O Colaborador não altera os dados. O caso de uso termina.

4a. O anúncio não possui candidaturas.
> 1. O caso de uso termina.

5a. Dados relativos à classificação de candidaturas não inseridos.
>	1. O sistema informa o sucedido.
>	2. O sistema permite a introdução dos dados em falta (passo 5)
>
	>	2a. O Colaborador não altera os dados. O caso de uso termina.

5a. Dados relativos aos Colaboradores intervenientes não inseridos.
>	1. O sistema informa o sucedido.
>	2. O sistema permite a introdução dos dados em falta (passo 7)
>
	> 2a. O Colaborador não altera os dados. O caso de uso termina.

9a. Dados relativos à data e hora de seriação não preenchidos.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 9)
>
	>	2a. O Colaborador não altera os dados. O caso de uso termina.


#### Requisitos especiais

-

#### Lista de Variações de Tecnologias e Dados

-

#### Frequência de Ocorrência

-

#### Questões em aberto

* Existirão novos tipos de regimentos manuais no futuro?
* Será possível o uso de regimentos manuais em anúncios onde a quantidade de candidaturas é demasiado elevada?
* O Colaborador será sempre o único capaz de executar a seriação?
* O que acontece quando um anúncio não possui candidaturas?
* O que acontece ao anúncio após o processo de seriação?
* A inserção dos dados relativos à data e hora poderão ser inseridos de maneira automática?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10_MD](UC10_MD.png)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1. O Colaborador inicia a seriação de candidaturas ao anúncio.|... interage com o utilizador? | SeriarAnuncioUI |Pure Fabrication |
| | ... coordena o UC? | SeriarAnuncioController | Controller |
|  | ...cria a instância de Seriaçao? | Plataforma | Creator (Regra1) |
 2. O sistema apresenta a lista de anúncios possíveis de seriação.| ...fornece a lista de anúncios? | Plataforma | IE: a Plataforma agrega os anúncios |
|3. O Colaborador seleciona o anúncio que tenciona seriar.||||
|4. O sistema apresenta a lista de candidaturas. |							 |             |                              |
|5. O Colaborador procede à classificação das candidaturas. |	...fornece a instância de candidatura? | Plataforma  | IE: a Plataforma agrega as candidaturas |
|  |...guarda os dados da classificação? | Seriaçao | Criada no passo 1 |
|6. O sistema apresenta uma lista de Colaboradores.|		 |    |    |
|7. O Colaborador seleciona os Colaboradores intervenientes na seriação.|...fornece a instância de Colaboradores? | Plataforma| IE: a Plataforma agrega os Colaboradores|
||...guarda os dados de seriação?|	Seriaçao |Criada no passo 1|
|8. O sistema pede para ser inserida a data e hora da seriação.||||
|9. O Colaborador introduz os dados.|...guarda os dados? |Seriaçao| Criada no passo 1|
|10. O sistema valida e apresenta os dados, pedindo que os confirme. |... valida os dados de seriação?(validação local)|Colaborador| IE: possui os seus próprios dados relativos ao processo de candidatura. |
| | ... valida os dados de seriação?(validação global) | Plataforma |IE: possui os dados relativos ao regimento. |  
|11. O Colaborador confirma. ||||
|12. O sistema regista os dados e informa o Colaborador do sucesso da operação.| ... guarda a seriação criada? | Plataforma | IE: a Plataforma agrega a seriação. |


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * SeriarAnuncio

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarAnuncioUI  
 * SeriarAnuncioController


###	Diagrama de Sequência

![UC10_SD.png](UC10_SD.png)


###	Diagrama de Classes

![UC10_CD.png](UC10_CD.png)
