# UC 7 - Registar Freelancer

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia o registo de um _Freelancer_. O sistema solicita os dados necessários (i.e. nome, NIF, endereço postal, contacto telefónico, email, habilitações académicas e reconhecimentos de competências técnicas. O administrativo  introduz os dados solicitados. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.


### SSD
![UC7_SSD](UC7_SSD.PNG)


### Formato Completo

#### Ator principal

* Administrativo

#### Partes interessadas e seus interesses



* **Administrativo:** pretende registar um _Freelancer_ para que este tenha acesso à plataforma.
* **_Freelancer_:** pretende ter acesso à plataforma.
* **T4J:** pretende que os _Freelancers_ se registem de modo a usar a plataforma.

#### Pré-condições

n/a

#### Pós-condições
A informação do registo é guardada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)


1. O administrativo inicia o registo de um _Freelancer_ .
2. O sistema solicita os dados necessários (i.e. nome, NIF, endereço postal, contacto telefónico, email, habilitações académicas e reconhecimentos de competências técnicas).
3. O administrativo introduz os dados solicitados .
4. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.
5. O administrativo confirma.
6. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da registo.

> O caso de uso termina.

4a. Dados de Endereço Postal incompletos.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4b. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4c. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

#### Requisitos especiais
(enumerar requisitos especiais aplicáveis apenas a este UC)

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

Sempre que o administrativo tencionar registar um novo _Freelancer_.

#### Questões em aberto

* Existem outros dados obrigatórios para além dos já conhecidos?
* Quais os dados que em conjunto permitem detetar a duplicação de _Freelancers_?
* É necessário existir algum mecanismo de segurança adicional para confirmar que a organização existe e é representada pela pessoa que a registou?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

(Apresentar aqui um excerto do MD relevante para este UC)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|O administrativo inicia o registo de um _Freelancer_ .|... interage com o utilizador?|RegistarFreelancerUI|Pure Fabrication|
|         |... coordena o UC?|RegistarOrganizacaoController||
|         |... cria instâncias de _Freelancer_ ?|||
| Passo2  		 |							 |             |                              |
| Passo3  		 |							 |             |                              |
| Passo4  		 |							 |             |                              |
| Passo5  		 |							 |             |                              |
| Passo6  		 |							 |             |                              |              

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Classe1
 * Classe2
 * Classe3

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * xxxxUI  
 * xxxxController


###	Diagrama de Sequência

![SD_UCX.png](SD_UCX.png)


###	Diagrama de Classes

![CD_UCX.png](CD_UCX.png)
