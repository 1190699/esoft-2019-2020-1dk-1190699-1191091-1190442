# UC8 - Publicar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a publicação de uma tarefa. O sistema apresenta uma lista de tarefas por si anteriormente criadas. O colaborador seleciona uma tarefa. O sistema apresenta a tarefa e solicita confirmação. O colaborador de organização confirma. O sistema solicita os dados necessários(período de publicação, período de apresentação das candidaturas,). O colaborador de organização introduz os dados solicitados. O sistema apresenta os dados e solicita confirmação. O colaborador de organização confirma. O sistema devolve um anúncio com os dados solicitados anteriormente.


### SSD
![UC8-SSD](UC8_SSD.svg)


### Formato Completo

#### Ator principal

Colaborador de organização

#### Partes interessadas e seus interesses

* **Colaborador de organização:** pretende publicar uma tarefa.
* **T4J:** pretende que as tarefas sejam publicadas.

#### Pré-condições

n/a

#### Pós-condições
Garante que um anúncio irá ser publicado na plataforma.

#### Cenário de sucesso principal (ou fluxo básico)


1. O colaborador de organização inicia a publicação de uma tarefa.
2. **O sistema vai buscar a organização da sessão atual e apresenta uma lista de tarefas por si anteriormente criadas.**
3. **O colaborador de organização seleciona uma tarefa.**
4. **O sistema apresenta uma lista com os tipos de regime aplicável.**
5. **O colaborador de organização seleciona um ripo de regime.**
6. O sistema solicita os dados necessários(período de publicação, período de apresentação das candidaturas e período de seriação e decisão da atribuição de tarefa).
7. O colaborador de organização introduz os dados solicitados.
8. O sistema valida e apresenta os dados solicitando confirmação.
9. O colaborador de organização confirma.
10. O sistema devolve um anúncio com os dados introduzidos.

#### Extensões (ou fluxos alternativos)

*. O colaborador de organização solicita o cancelamento da publicação da tarefa.
>O caso de uso termina.

2a. Não existem tarefas definidas no sistema.
>1. O sistema informa o colaborador de organização. O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>1. O sistema informa da falta de dados.
>2. O sistema permite a introdução dos dados em falta.
>
      >2a. O colaborador de organização não introduz os dados em falta. O caso de uso termina

6b. O sistema deteta que os dados introduzidos são inválidos.
> 1. O sistema alerta o colaborador de organização para o facto.
> 2. O sistema permite a sua alteração.
>
    >2a. O colaborador de organização não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
* Existem outros dados necessários ?
* Todos os dados são obrigatórios?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC8-MD](MD_UC8.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| O colaborador de organização inicia a publicação de uma tarefa. 		 |	interage com o colaborador?						 | PublicarTarefaUI             |Pure Fabrcation   |
||coordena o UC?|PublicarTarefaController |Controller |
||cria instância de Anuncio ? | RegistoAnuncio| High cohesion e Low coupling.
| O sistema vai buscar a organização da sessão atual apresenta uma lista de tarefas por si anteriormente criadas. | conhece a lista?							 | Organizacao            |  A classe Organizacao no MD possui a classe Tarefa. |
||vai buscar a organização?| RegistoOrganizacao|High cohesion e low coupling.|
| O colaborador de organização seleciona uma tarefa.  | guarda a tarefa selecionada?       | RegistoTarefa       | High cohesion e Low coupling. |
| O sistema apresenta uma lista com os tipos de regime aplicaveis.	|conhece a lista ?				 |Seriacoes             |É lá que estão os tipos de seriações.                    |
| O colaborador de organização seleciona| guarda a escolha ? |Seriacoes | é lá que estão o tipo de regimes aplicaveis.|
|O sistema solicita os dados necessários(período de publicação, apresentação e período de seriação)|||
|O colaborador introduz os dados| guarda os dados?| RegistoAnuncio|High cohesion e Low coupling|  
| O sistema valida e apresenta os dados solicitando confirmação.  		 |valida a escolha(validação local)?							 | Anuncio    | Seus próprios dados.                              |
||valida a escolha(validação global)?|RegistoAnuncio| High cohesion e Low coupling.|
|O colaborador de organização confirma.|||
|O sistema devolve um anúncio com os dados introduzidos.| vai devolver o anúncio?|Anuncio |É classe que tem todos os dados para devolver o anúncio.|

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organizacao
 * PublicarTarefa
 * Seriacoes

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * PublicarTarefaUI  
 * PublicarTarefaController
 * RegistoTarefa
 * RegistoAnuncio
 * RegistoOrganizacao


###	Diagrama de Sequência

![SD_UC8.svg](SD_UC8.svg)
![SD2_UC8.svg](SD2_UC8.svg)


###	Diagrama de Classes

![CD_UC8.svg](CD_UC8.svg)
