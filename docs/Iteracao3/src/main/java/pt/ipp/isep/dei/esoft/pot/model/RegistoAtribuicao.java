package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RegistoAtribuicao {
    public class RegistoAtribuicao {

        private String atribID;
        private Date dataAtrib;
        private List<Atribuicao> listaAtribuicao;

        public ListaAtribuicao(String emailFree, Freelancer free, String descInf, int duracao, double valor, Anuncio anuncioId){
            atribID = geraAtribID();
            dataAtrib = geraDataAtrib();
            new Atribuicao(emailFree,free,descInf,duracao,valor,anuncioId,atribID,dataAtrib);
        }

        public String geraAtribID(){
            //código para gerar um código único
            return atribId;
        }

        public boolean validaAtribuicao(Atribuicao atrib){
            for(List<Atribuicao> a : listaAtribuicao){
                if(atrib.getAtribID().equals(a.getAtribID())){
                    return false;
                }
            }
            return true;
        }

        public void addAtribuicao(Atribuicao atrib){
            if(!listaAtrib.contains(atrib)){
                listaAtrib.add(atrib);
            }
        }

        public boolean registaAtribuicao(Atribuicao atrib){
            if (validaAtribuicao(atrib)) {
                return addAtribuicao(atrib);
            }
            return false;
        }

        public Atribuicao novoProcessoAtribuicao(String emailFree, Freelancer free, String descInf, int duracao, double valor, Anuncio anuncioId) {
            return new Atribuicao(emailFree,free,descInf,duracao,valor,anuncioId);
        }


}
