package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;

public class Atribuicao {
    private Freelancer free;
    private String email;
    private String descInf;
    private int duracao;
    private double valor;
    private Anuncio anuncioId;
    private String atribID;
    private Date dataAtrib;

    public Atribuicao(String email, Freelancer free, String descInf, int duracao, double valor, Anuncio anuncioId, String atribID, Date dataAtrib){
        if(email == null || free == null || descInf == null || duracao == 0 || valor == 0 || anuncioId == null || atribID == null || dataAtrib == null){
            System.out.println("Os dados não podem ser nulos!");
        }
        this.email = email;
        this.free = free;
        this.descInf = descInf;
        this.duracao = duracao;
        this.valor = valor;
        this.anuncioId = anuncioId;
        this.atribID = atribID;
        this.dataAtrib = dataAtrib;
    }

    public Freelancer getFree() { return free; }

    public String getEmail() { return email; }

    public String getDescInf() { return descInf; }

    public int getDuracao() { return duracao; }

    public double getValor() { return valor; }

    public Anuncio getAnuncioId() { return anuncioId; }

    public String getAtribID() { return atribID; }

    public void setAtribID(String atribID) { this.atribID = atribID; }

    public Date getDataAtrib() { return dataAtrib; }

    public void setDataAtrib(Date dataAtrib) { this.dataAtrib = dataAtrib; }

    public Atribuicao validaAtribuicao(Atribuicao atrib){
        ///valida a Atribuicao
        return atrib;
    }

}
