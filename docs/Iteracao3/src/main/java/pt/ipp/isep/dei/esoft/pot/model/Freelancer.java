package pt.ipp.isep.dei.esoft.pot.model;

public class Freelancer {
    private String nome;
    private String NIF;
    private String telefone;
    private String email;

    /**
     * Gets nome.
     *
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets nome.
     *
     * @param nome the nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Gets nif.
     *
     * @return the nif
     */
    public String getNIF() {
        return NIF;
    }

    /**
     * Sets nif.
     *
     * @param NIF the nif
     */
    public void setNIF(String NIF) {
        this.NIF = NIF;
    }

    /**
     * Gets telefone.
     *
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * Sets telefone.
     *
     * @param telefone the telefone
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }
}

