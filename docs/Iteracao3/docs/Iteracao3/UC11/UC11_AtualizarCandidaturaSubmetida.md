# UC13 - Seriar Automaticamente Candidaturas De Anúncios

## 1. Engenharia de Requisitos

### Formato Breve

   O freelancer inicia a atualização de uma candidatura já submetida. 
 O sistema mostra as informações atualizáveis para o freelancer selecionar. 
 O freelancer seleciona uma das opções. O sistema exibe a informação da opção 
selecionada e pede ao freelancer para inserir a atualização da informação. 
 O freelancer insere o conteúdo atualizado. O sistema exibe a informação atual 
e solicita a confirmação da atualização. O freelancer confirma a atualização. 
 O sistema confirma o sucesso da operação.

### SSD
![UC11_SSD.svg](UC11_SSD.svg)


### Formato Completo

#### Ator principal

* **Freelancer**

#### Partes interessadas e seus interesses

* **Freelancer:** pretende atualizar uma candidatura previamente efetuada e que está registada no sistema.

#### Pré-condições

* O Freelancer ter efetuado pelo menos uma candidatura que esteja registada na Plataforma.

#### Pós-condições

* O Freelancer modifica a candidatura que selecionou.

#### Cenário de sucesso principal (ou fluxo básico)

1.  O freelancer inicia a atualização de uma candidatura já submetida. 
2.  O sistema mostra a lista de candidaturas efetuadas pelo freelancer dentro do período de candidatura.
3.  O freelancer seleciona uma candidatura para atualizar.
4.  O sistema exibe a candidatura selecionada e pede ao freelancer para inserir a atualização da informação.
5.  O freelancer insere o conteúdo atualizado.
6.  O sistema exibe a informação atual e solicita a confirmação da atualização. 
7.  O freelancer confirma a atualização. 
8.  O sistema confirma o sucesso da operação.


#### Extensões (ou fluxos alternativos)

a. O freelancer solicita o cancelamento da atualização da candidatura.
> O caso de uso termina.

2a. Não ha candidaturas dentro do período de candidatura.
> O caso de uso termina.

6a.Nenhum dado foi alterado.
>1. O sistema informa que nenhum dado foi alterado.
>2. O sistema permite a introdução dos dados que o freelancer pretende atualizar.
>>2a. O freelancer não introduz os dados novos. O caso de uso termina.
 



#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Qual é a frequência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC11_MD.svg](UC11_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O *Freelancer* inicia o processo de atualizar uma candidatura.  |	... interage com o Freelancer? | AtualizarCandidaturaUI | IE:Pure Fabrication |
|  		         |... coordena o UC?| AtualizarCandidaturaController | IE:Controller|
|                |... atualiza as instâncias de Candidatura?| ListaCandidaturas | IE: atualiza a instância candidatura.|
| | ...conhece o utilizador/Freelancer a usar o sistema?|SessaoUtilizador | IE: cf. documentação do componente de gestão de utilizadores. |
| | ...conhece o Freelancer?| Plataforma| conhece todos os Freelancers|
| | |RegistoFreelancer | Por aplicação de HC+LC delega a RegistoFreelancer|
| 2. O Sistema mostra a lista de candidaturas efetuadas pelo *Freelancer* dentro do período de candidatura.  		 |	... conhece as listas de Candidatura? | Anúncio | IE: Anuncio tem listaCandidaturas |
| 3. O *Freelancer* seleciona a candidatura que pretende atualizar.  		 |		    |       |                      |
| 4. O sistema apresenta a candidatura selecionada e solicita os novos dados.  		 |							 |             |                              |
| 5. O *Freelancer* introduz os novos dados.    		 |	... guarda os dados introduzidos? | Candidatura | IE: Candidatura conhece os seus dados |
| | | ListaCandidaturas| Por aplicação de HC+LC delega a ListaCandidaturas |
| 6. O Sistema valida e solicita a confirmação do *Freelancer* para modificar a candidatura.  		 |	... valida os dados da Candidatura (validação local)?			 |  Candidatura  |  IE: possui os seus próprios dados.   |              
| |... valida os dados da Candidatura (validação global)? | ListaCandidaturas | IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas
|
| 7. O *Freelancer* confirma os dados que introduziu.  		 |							 |             |                             |             
| 8. O sistema regista os dados novos e atualiza a Candidatura e informa o Freelancer do sucesso da operação. |	...guarda a Candidatura atualizada? | Candidatura | IE: no MD a Candidatura é atualizada. |    
|  |	...guarda a Candidatura atualizada? | ListaCandidaturas| IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas |    
          

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 
 * Candidatura
 * Anúncio


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AtualizarCandidaturaUI  
 * AtualizarCandidaturaController
 * ListaCandidaturas

###	Diagrama de Sequência

![UC11_SD.svg](UC11_SD.svg)

![UC11_SD_getCandidaturasFreelancer.svg](UC11_SD_getCandidaturasFreelancer.svg)


###	Diagrama de Classes

![UC11_CD.svg](UC11_CD.svg)

