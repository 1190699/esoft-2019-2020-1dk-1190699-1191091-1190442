# UC13 - Seriar Automaticamente Candidaturas De Anúncios

## 1. Engenharia de Requisitos

### Formato Breve

O Tempo/relógio inicia o processo automático de seriação ao atingir o tempo 
estipulado, identificando o anúncio cujo tipo de regimento estipula que os 
critérios de seriação sejam objetivos, que estejam no período de seriação e 
que ainda não tenham sido seriados.


### SSD

![UC13_SSD.svg](UC13_SSD.svg)


### Formato Completo

#### Ator principal

* Tempo/Relógio

#### Partes interessadas e seus interesses

* **Freelancer:** pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* **Organização:** pretende contratar pessoas externas (outsourcing) para a realização de determinadas tarefas e com competências técnicas apropriadas.
* **T4J:** pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa.

#### Pré-condições

* Existir pelo menos um anúncio de tarefa em condições de ser seriado manualmente pelo 
colaborador ativo no sistema.

#### Pós-condições

\-

#### Cenário de sucesso principal (ou fluxo básico)

1.  O Tempo/relógio inicia o processo automático de seriação e de atribuição dos 
candidatos quando chega ao horário estabelecido.

2. O sistema identifica os anúncios que estão assentes em critérios objetivos, em que 
a atribuição está especificada pelo regimento como sendo obrigatória, que estejam em 
período de seriação e que ainda não tenham sido seriados. O sistema inicia o processo 
de seriação automática e o processo de atribuição automática. O sistema regista  os 
dados e termina o processo.

#### Extensões (ou fluxos alternativos)

*2a. Não existem anúncios em que o tipo de regimento estipula que os critérios da seriação são objetivos.
>"O caso de uso termina."

*2b. Todos os anúncios ja estavam seriados antes do início do processo automático.
>"O caso de uso termina."

*2c. Não existem anúncios no período de seriação.
>"O caso de uso termina."

*2d. A atribuição é opcional.
>"O caso de uso termina."

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

\-

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC13_MD.svg](UC13_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1. O horário de seriação automática é atingido | ...coordena o UC?     |   SeriarCandidaturaAnuncioTask       |      Task  |
|       |   ... determina que o tempo foi atingido e despoleta o processo de seriação automática? |     **Timer**       |   Timer        |
|       | ...cria a instância do Timer?  | **Plataforma**|   Creator: Plataforma conhece a hora em que a seriação deve começar.        |
|2. O sistema identifica o anúncio cujo tipo de regimento estipula que os critérios de seriação sejam objetivos, que estejam no período de seriação e que ainda não tenham sido seriados. Posteriormente, inicia o processo de seriação automática. | ...conhece RegistoAnuncio de tarefas publicadas?		| **Plataforma** | IE: Plataforma contém/agrega RegistoAnuncio (segundo Padrão HC+LC sobre Plataforma)  |       
|       | ...possui a candidatura?   | **ListaCandidaturas**      | IE: Lista de Candidaturas contem candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas |
|       | ...conhece a classe Anuncio? |  **RegistoAnuncio** | **IE: RegistoAnuncio contém/agrega Anuncio (segundo Padrão HC + LC)** |
|       | ...conhece a classe ProcessoSeriacao? |**Anuncio**| IE:o Anuncio conhece os seus processos de seriação |
|       | ...cria a instância do processo de seriação?  | **Anuncio**    |  Creator (Regra1): no MD o anuncio espoleta o processo de seriação |
|       | ...cria, valida e adiciona a instancia da classificação? | **ProcessoSeriacao** | creator: no MD a classificacao é resultante do processo de seriação |
|       |       |**Anúncio**|IE: no MD o Anúncio recebe as seriações.   |
|       | ...regista o processo de seriação? | **Anuncio** | IE: o Anuncio contém os processos de seriação. |
|       | ...conhece o tipo de regimento|**Anuncio**|IE: no MD o Anúncio recebe TipoRegimento.  |
|       | ... verifica a obrigatoriedade da atribuição?    | **TipoRegimento**       | IE: o Tipo de Regimento contém a obrigatoriedade. |   
|       | ...cria a instância do Processo de Atribuição?    | **RegistoAnuncios** | creator: no MD o processo de adjudicacao está associado ao anuncio |
|       | ...possui o Freelancer? | **Candidatura** | IE: no MD a candidatura é efetuada pelo Freelancer |
|       | ...possui o Registo de Organizacao? | **Plataforma** | IE:a plataforma possui organizacao. |
|       | ...possui Organizacao? | **RegistoOrganizacoes** | IE: RegistoOrganizacao contém/agrega Organizacao (segundo Padrão HC + LC)
|       | ...possui a tarefa? | **Anuncio** | IE: anuncio tem tarefa |
|       | ...cria a Atribuição | **Anuncio** | creator: Anuncio tem atribuição |
|       | ...valida a Atribuição? | **Atribuicao** |  Creator (Regra1): no MD o anuncio espoleta o processo de seriação   | 
|       | ...vai guardar a atribuição?|**ProcessoAtribuicao**| IE: o ProcessoAtribuicao contém as atribuições. |
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * ProcessoSeriacao
 * Candidatura
 * ProcessoSeriacao
 * TipoRegimento
 * Classificacao

Outras classes de software (i.e. Pure Fabrication) identificadas:
  
 * SeriarCandidaturaAnuncioTask
 * RegistoAnuncios
 * RegistoOrganizacoes
 * ListaCandidaturas

 
###	Diagrama de Sequência

![UC13_SD.svg](UC13_SD.svg)

![UC13_SD_seriarCandidaturasSubmetidas.svg](UC13_SD_seriarCandidaturasSubmetidas.svg)

![UC13_SD_atribuirCandidaturasSubmetidas.svg](UC13_SD_atribuirCandidaturasSubmetidas.svg)

###	Diagrama de Classes

![UC13_CD.svg](UC13_CD.svg)

