# UC 12 - Retirar Candidatura Submetida

## 1. Engenharia de Requisitos

### Formato Breve

O _Freelancer_ pretende retirar uma candidatura anteriormente submetida a um anúncio. O sistema apresenta a lista de candidaturas submetidas pelo _Freelancer_. O _Freelancer_ seleciona a candidatura a retirar. O sistema valida a seleção e pede que confirme. O _Freelancer_ confirma. O sistema retira a candidatura e informa do sucesso da operação.


### SSD
![UC12_SSD.png](UC12_SSD.png)


### Formato Completo

#### Ator principal

* _Freelancer_

#### Partes interessadas e seus interesses

* **_Freelancer_:** pretende poder remover candidaturas por si efetuadas.
* **Colaborador:** pretende que os _Freelancers_ possam remover candidaturas aos anúncios por si criados.
* **T4J:** pretende que os _Freelancers_ possam proceder ao processo de remoção de candidaturas indesejadas.

#### Pré-condições

* O _Freelancer_ deve estar registado no sistema.

#### Pós-condições

* O anúncio selecionado pelo _Freelancer_ deve ser retirado do processo de candidaturas.

#### Cenário de sucesso principal (ou fluxo básico)

1. O Freelancer pretende retirar uma candidatura anteriormente submetida a um anúncio.
2. O sistema apresenta a lista de candidaturas submetidas pelo Freelancer.
3. O Freelancer seleciona a candidatura a retirar.
4. O sistema valida a seleção e pede que confirme.
5. O Freelancer confirma.
6. O sistema retira a candidatura e informa do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O _Freelancer_ solicita o cancelamento da remoção da candidatura.
> "O caso de uso termina"

2a. Não existe nenhuma candidatura realizada pelo _Freelancer_.
> "O caso de uso termina"

2b. As candidaturas realizadas pelo _Freelancer_ encontram-se fora da data do fim de candidatura.
> "O caso de uso termina"

4a. Nenhuma candidatura selecionada.
>	1. O sistema informa que não existe nenhuma candidatura selecionada.
>	2. O sistema permite a seleção de uma candidatura (passo 3)
>
	>	2a. O _Freelancer_ não seleciona nenhuma candidatura. O caso de uso termina.


#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

* Sempre que um _Freelancer_ pretender retirar uma candidatura anteriormente submetida.

#### Questões em aberto

1. O _Freelancer_ poderá retirar mais que uma candidatura de uma vez?
2. Será preciso fornecer algum tipo de justificação para retirar uma candidatura?
3. O _Freelancer_ poderá retirar a candidatura a qualquer momento antes do processo de seriação?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC12_MD.png](UC12_MD.png)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Freelancer pretende retirar uma candidatura anteriormente submetida a um anúncio. |... interage com o utilizador? | RetirarCandidaturaUI | Pure Fabrication |
|  | ... coordena o UC?  | RetirarCandidaturaController | Controller |
|  | ...conhece o utilizador a usar o sistema? | SessaoUtilizador | IE: cf. documentação do componente de gestão de utilizadores.|
|  | ...conhece o _Freelancer_? | Plataforma | Conhece todos os Freelancers |
|  |  | RegistoFreelancer | Por aplicação de HC+LC delega a RegistoFreelancer |
| 2. O sistema apresenta a lista de candidaturas submetidas pelo Freelancer. |	...possui as candidaturas? | Plataforma | IE: a Plataforma possui as candidaturas.|
|||RegistoAnuncios|IE: no MD a Plataforma possui Anuncios. Por aplicação de HC+LC delega a RegistoAnuncios|
|  |  | ListaCandidaturas | IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas |
| | ...verifica se essa candidatura é possivel de retirar? | Anúncio | IE: no MD o Anúncio recebe Candidaturas e possui as datas relativas ao fim do período de candidatura|
| 3. O Freelancer seleciona a candidatura a retirar. |  |  |  |
| 4. O sistema valida a seleção e pede que confirme. | | |  |
| 5. O Freelancer confirma. | |  |  |
| 6. O sistema retira a candidatura e informa do sucesso da operação. |...retira a candidatura selecionada? | ListaCandidaturas | IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas  |
| | | Candidatura | IE: irá ser retirada do sistema|
| | |Anúncio| IE: no MD o Anúncio recebe Candidaturas. |
|  | ...informa o _Freelancer_? | RetirarCandidaturaUI |  |              

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anúncio
 * Candidatura

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RetirarCandidaturaUI  
 * RetirarCandidaturaController
 * ListaCandidaturas
 * RegistoFreelancer

 Outras classes de sistemas/componentes externos:
 * SessaoUtilizador


###	Diagrama de Sequência

![UC12_SD.png](UC12_SD.png)

* SD_getAnunciosComFreelancer(fr)

![UC12_SD_REF.png](UC12_SD_REF.png)

###	Diagrama de Classes

![UC12_CD.png](UC12_CD.png)
