# UC14 - Adjudicar Anúncio (Manualmente)

## 1. Engenharia de Requisitos

### Formato breve
O gestor inicia a abjudicação manual de um anúncio. O sistema solicita os dados necessários(a organização que adjudica a tarefa, o freelancer a quem é adjudicada a realização da tarefa, a descrição da tarefa adjudicada, o período afeto á realização da mesma, o valor remuneratório aceite por ambas as partes e uma referência ao anúncio que lhe deu origem). O gestor introduz os dados solicitados. O sistema apresenta os dados solicitando confirmação. O gestor confirma. O sistema gera um número único sequencial(por ano) e atribui também a data em que a adjudicação ocorreu, regista os dados da atribuição e informa o gestor do sucesso da operação.


### SSD
![UC14-SSD.svg](UC14_SSD.svg)


### Formato Completo

#### Ator principal

Gestor de organização

#### Partes interessadas e seus interesses

* **Gestor:** pretende adjudicar manualmente um anúncio.  
* **Organização:** pretende contratar um freelancer para realizar a tarefa.
* **Freelancer:** pretende ser contratado para realizar alguma tarefa.
* **T4J:** pretende que a sua aplicação seja utilizada.

#### Pré-condições
n/a

#### Pós-condições
Os dados são registados.

#### Cenário de sucesso principal (ou fluxo básico)


1. O gestor inicia a abjudicação manual de um anúncio.
2. O Sistema vai buscar a organização do colaborador, mostra uma lista de anúncios disponíveis já referênciados e solicita a escolha de um deles.
3. O gestor seleciona um anúncio.    
4. O sistema, apresenta uma sugestão de um freelancer que mais é adequado à adjudicação do anúncio.
5. O gestor escolhe o freelancer sugerido.
6. O sistema vai buscar o período afeto á realização da tarefa e o valor remuneratório associados ao freelancer.  
7. O gestor vê os dados apresentados e uma possível adjudicação do anúncio.
8. O sistema valida e apresenta os dados todos solicitando confirmação.
9. O gestor confirma.
10. O sistema gera um número único sequencial(por ano) e atribui também a data em que a adjudicação ocorreu, regista os dados da atribuição e informa o colaborador do sucesso da operação.


#### Extensões (ou fluxos alternativos)

3a. Escolhe um anúncio inválido ou inexistente.
 > 1. O sistema infomra o gestor do erro.
 > 2. O sistema permite a reeintrodução dos dados do anúncio.
  >> 2a. O gestor não altera os dados. O caso de uso termina.

5a. Escolhe o freelancer que não é sugerido.
>1. O sistema informa o gestor do erro.
>2. O sistema permite a escolha do freelancer escolhido.
  >> 2a. O gestor não altera a sua escolha. O caso de uso termina.

7a. Introduz dados inválidos ou números negativos.
>1. O sistema informa o gestor do erro.
>2. O sistema permite a reeintrodução dos dados(período e o valor remuneratório.)
 >>2a. O gestor não altera os dados. O caso de uso termina.

#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
* Existem mais dados obrigatórios para além dos já especificados?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC14_MD.svg](UC14_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| O gestor inicia a adjudicação manual de um anúncio.	| interage com o utilizador?							 | AdjudicarAnuncioUI            | Pure Fabrication                             |
||coordena o UC?|AdjudicarAnuncioController|Controller|
||conhece o gestor ?| SessaoUtilizador|IE: documentação do componete de gestão de utilizadores.|
|   	O Sistema vai buscar a organização do colaborador, mostra uma lista de anúncios disponíveis já referênciados e solicita a escolha de um deles.	 |	sabe a que organização o colaborador pertence?						 | RegistoOrganizacao            |IE: conhece todas as organizações.|
|||Organizacao|IE: conhece os seus colaboradores.|
||conhece o RegistoOrganizacao?|Plataforma|IE: Plataforma tem RegistoOrganizacao|
||conhece os anúncios?|Plataforma|IE:Plataforma possui Anuncio.|
|||ListaAnuncio|IE: no MD a Plataforma possui ListaAnuncio.Por aplicação de HC+LC delega a ListaAnuncio.|
||cria a instância AdjudicarAnuncio|RegistoAdjudicarAnuncio|Creator(Regra1):no MD a Plataforma adjudica AdjudicarAnuncio e por HC+LC delega a RegistoAdjudicarAnuncio.|							   
| O gestor seleciona um anúncio.	 | guarda o anúncio selecionado?							 | AdjudicarAnuncio            |   IE                            |
|O sistema apresenta uma sugestão de um freelancer que mais é adequado à adjudicação do anúncio. | conhece os freelancers?							 |Plataforma             |IE: no MD a Plataforma tem/usa Freelancer.|
|||ListaCandidatura|IE: no MD a Plataforma tem/usa Freelancer.Por HC+LC delega a ListaFreelancer|
||trata de fazer a sugestão do freelancer mais adequado a adjudicar o anúncio?|TipoRegimento|TipoRegimento rege a classe Anuncio, que depois irá ser adjudicada.|
|  O gestor escolhe o freelancer sugerido.		 |	guarda a escolha?						 | AdjudicarAnuncio           | IE                              |
|   O sistema vai buscar o período afeto à realização da tarefa e o valor remuneratório associados ao freelancer.	|	conhece o período e o valor remuneratório?						 | Candidatura        |IE: possui os seus próprios dados.                              |              
| O gestor vê os dados apresentados e uma possível adjudicação do anúncio.|guarda os dados que o sistema foi buscar?|AdjudicarAnuncio|AdjudicarAnuncio conhece os seus dados. |
|O sistema valida e apresenta os dados todos solicitando confirmação.|valida os dados da AdjudicarAnuncio(validação local)?|AdjudicarAnuncio|IE: possui os seus próprios dados.|
||valida os dados da AdjudicarAnuncio(validação global)?|RegistoAdjudicarAnuncio|IE:por delegação da Plataforma.|
|O gestor confirma.||||
|O sistema gera um número único sequencial(por ano) e atribui também a data em que a adjudicação ocorreu, regista os dados da atribuição e informa o colaborador do sucesso da operação.|guarda a AdjudicarAnuncio|RegistoAdjudicarAnuncio|IE|
||gera o número único sequencial e atribui a data em que adjudicação ococrreu ?|AdjudicarAnuncio|IE: possui os seus próprios dados. |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * AdjudicarAnuncio
 * Organizacao
 * Anuncio
 * TipoRegimento

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AdjudicarAnuncioUI  
 * AdjudicarAnuncioController
 * RegistoAdjudicarAnuncio
 * RegistoOrganizacao
 * ListaAnuncio
 * ListaCandidatura


Outras classes de sistemas/componetes externos:
* SessaoUtilizador

###	Diagrama de Sequência

![SD_UC14.svg](SD_UC14.svg)
![SD_getAnunciosParaAdjudicar.svg](SD_getAnunciosParaAdjudicar.svg)

###	Diagrama de Classes

![UC14_CD.svg](UC14_CD.svg)
