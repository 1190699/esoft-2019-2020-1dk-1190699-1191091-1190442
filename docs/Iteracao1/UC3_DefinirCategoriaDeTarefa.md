# UC 3 - Definir Categoria de Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia a definição de uma nova categoria de tarefa. O sistema solicita os dados (i.é. descrição, área de atividade e lista de competências). O administrativo introduz os dados. O sistema apresenta e valida os dados. o administrativo confirma. O sistema regista a nova categoria de tarefa e dá a operação como bem sucedida.


### SSD
![UC3_SSD.PNG](UC3_SSD.PNG)


### Formato Completo

#### Ator principal

Administrador

#### Partes interessadas e seus interesses

* **Administrador:** Pretende definir a categoria de tarefa de modo a facilitar a escolha de pessoas relacionadas com a categoria mencionada.
* **T4J:** Pretende que a organização em causa catalogue as diferentes áreas de atividade.


#### Pré-condições

Existência de Áreas de atividades e competências técnicas

#### Pós-condições
A informação da Categoria de Tarefa é registada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a definição de uma nova Categoria de Tarefa.
2. O sistema solicita os dados necessários (i.e. descrição, área de atividade, lista de competências) e gera um identificador interno.
3. O administrativo introduz os dados solicitados.
4. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.  
5. O administrativo confirma.  
6. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O utilizador não registado solicita o cancelamento do registo.
> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
> 1.	O sistema informa quais os dados em falta.
> 2.	O sistema permite a introdução dos dados em falta (passo 3)
>>2a. O administrativo não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

> 1.	O sistema alerta o administrativo para o facto.
> 2.	O sistema permite a sua alteração (passo 3).
>>2a. O administrativo não altera os dados. O caso de uso termina.

4c. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.

> 1.	O sistema alerta o utilizador não registado para o facto.
> 2.	O sistema permite a sua alteração (passo 3)
>>2a. O utilizador não registado não altera os dados. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
1. Existem outros dados obrigatórios para além dos já conhecidos?
2. Quais os dados que em conjunto permitem detetar a duplicação de Categorias de Tarefas ?
3. Qual a frequência de ocorrência deste caso de uso?
4. O código é sempre gerado automaticamente?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC3_MD.PNG](UC3_MD.PNG)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia a definição de uma nova categoria de tarefa.  |... interage com o utilizador?	 |  DefinirCategoriaTarefaUI |  Pure Fabrication    |
|	  |...coordenar o UC?							 |  DefinirCategoriaTarefaController  |       Controller      |
|		 |...criar instancia de Categoria de tarefa			 |   Plataforma  |  Creator (Regra1) |
|2. O sistema solicita a descrição da categoria de tarefa.  |			 |    |     |
|3. O administrativo introduz a categoria de tarefa.  		 |	...guardar os dados introduzidos?					 | CategoriaTarefa | IE: instância criada no passo 1 |
|4. O sistema solicita a área de atividade.|   |    |
|5. O administrativo define uma área de atividade.  | ...tem as áreas de atividade guardadas?  | AreaAtividade  | Objeto da UC2 |
|6. O sistema solicita a lista de competências.  |  |  |  |
|7. O administrativo adiciona as competências. | ...guarda os dados?| CategoriaTarefa |IE: instância criada no primeiro passo |
| | 	...valida os dados da Area (validação local) | CategoriaTarefa | IE: validação local |
|  | 	...valida os dados da Area (validação global) | CategoriaTarefa |  IE: validação global  |
|8. O sistema regista os dados e informa o administrativo do sucesso da operação. |...guarda a Categoria de Tarefa?	 |Plataforma | IE: no MD a Plataforma tem ou agrega CategoriaTarefa|


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * CategoriaTarefa

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirCategoriaTarefaUI  
 * DefinirCategoriaTarefaController


###	Diagrama de Sequência

![UC3_SD.jpg](UC3_SD.jpg)


###	Diagrama de Classes

![UC3_CD.PNG](UC3_CD.PNG)
