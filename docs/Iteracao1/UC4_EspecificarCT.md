# UC4 - Especificar Competência Técnica

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia a especificação de uma competência técnica. O sistema solicita os dados (e.g. código, descrição breve, descrição detalhada e a área de atividade.) O administrativo introduz os dados que lhe são solicitados. O sistema apresenta os dados e solicita confirmação. O administrador confirma. O sistema demonstra o sucesso da operação.    


### SSD
![SSD_UC4.svg](SSD_UC4.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses


* **Administrativo:** especifica uma competência técnica para depois ser catalogada com categorias de tarefas nas áreas de atividade.
* **T4J:** pretende que a plataforma permita catalogar as competências técnicas e as suas categorias de tarefas na sua área de atividade.

#### Pré-condições

É necessário já ter sido criada uma área de atividade referente a essa competência técnica.

#### Pós-condições
A informação da competência técnica é registada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)


1. O administrativo inicia a especificação de uma competência técnica.
2. O sistema apresenta a lista das áreas de atividade.
3. O administrativo escolhe a área.
4. O sistema valida e apresenta a escolha, solicitando confirmação.
5. O administrativo confirma.
6. O sistema solicita os dados necessários sobre a organização(i.e. código, descrição breve, descrição detalhada).
7. O administrativo introduz os dados solicitados.
8. O sistema valida e apresenta os dados, solicitando que os confirme.
9. O administrativo confirma.
10. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*.O administrativo solicita o cancelamento da especificação da competência técnica.

>O caso de uso termina.

4a. Área de atividade não existe.
>1. O sistema informa que área de atividade não existe.
>2. O sistema permite a introdução de uma nova área de atividade(passo 3)
>>
  >>    2a. O administrativo não altera os dados. O caso de uso termina.

4b. Dados mínimos obrigatórios em falta.
>1. O sistema informa da falta de dados.
>2. O sistema permite a introdução dos dados em falta.
>>
  >>  2.a O administrativo não altera os dados. O caso de uso termina.

4c. O sistema deteta que os dados(ou algum subconjunto de dados) introduzidos devem ser únicos e que já existem no sistema.
>1. O sistema avisa o administrativo do sucedido.
>2. O sistema permite a sua alteração.
>>
  >>2a. O administrativo não altera os dados. O caso de uso termina.

5a. O administrativo não confirma.
>1.O sistema permite a introdução de novos dados para a especificação de uma nova área de atividade
>>
  >>2a. O administrativo não altera os dados. O caso de uso termina.

9a.O administrativo não confirma.
>1. O sistema permite a introdução de novos dados para a especificação de novos dados(cod, dsBreve,dsDet).
>>
   >>2a. O administrativo não altera os dados. O caso de uso termina.    

#### Requisitos especiais
\-
#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
Sempre que um administrativo desejar especificar uma competência técnica nova.

#### Questões em aberto

Será necessário a introdução de mais alguns dados obrigatórios?   
Quais os dados que ao serem repetidos iram mostrar a duplicação de dados?                                                               
Quais as condições necessárias?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![SSD_UC4.svg](UC4_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| O administrativo inicia a especificação de uma competência técnica.		 |	interage com o administrativo?|EspecificarCompetenciaTecnicaUI|Pure Fabrication|
| | coordena o UC?| EspecificarCompetenciaController| Controller |
| |cria instâncias da competência técnica?					 |  Plataforma  | Creator                             |
 |O sistema apresenta a lista das áreas de atividade. |apresenta a lista das áreas de atividade? |ListarAreasAtividadeUI |Pure Fabrication   |
|O administrativo escolhe a área |  |   |
|O sistema valida e apresenta a escolha, solicitando confirmação|valida a área de atividade?|Plataforma|A plataforma tem registadas áreas de atividade|
|O administrativo confirma| | | |
| O sistema solicita os dados necessários sobre a organização(i.e. código, descrição breve, descrição detalhada). 		 | |             |                              |
| O administrativo introduz os dados solicitados.   		 | guarda os dados introduzidos?          |CompetenciaTecnica              | instancia criada no primeiro passo                              |
| O sistema valida e apresenta os dados solicitados.  		 |valida os dados da competência técnica(validação local) ?| CompetenciaTecnica|   possui os seus próprios dados|
| |valida os dados da Competência técnica(validação global)? 		|   Plataforma         | A Plataforma tem registadas Competências técnicas                             |
| O administrativo confirma.  		 |							 |             |                              |
| O sistema regista os dados e informa do sucesso da operação.  		 |guarda a competência técnica criada?							 |Plataforma             | No MD a Plataforma tem competência técnica                              |              

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * AreaAtividade
 * CompetenciaTecnica

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarCompetenciaTecnicaUI  
 * EspecificarCompetenciaController


###	Diagrama de Sequência

![UC4_SD.svg](UC4_SD.svg)


###	Diagrama de Classes

![CD_UC4.svg](CD_UC4.svg)
