# UC6 - Especificar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a definição de uma nova tarefa. O sistema solicita os dados necessários (i.e. referencia, designação, descrição informal, descrição de caracter técnico, estimativa de duração, estimativa de custo, categoria em que se enquadra).O colaborador de organização introduz os dados solicitados. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O colaborador de organização confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

### SSD
![UC6_SSD.png](UC6_SSD.png)


### Formato Completo

#### Ator principal

Colaborador da Organização

#### Partes interessadas e seus interesses
* **Colaborador da Organização:** Pretende registar uma nova tarefa.
* **T4J:** Pretende que a organização em causa catalogue diferentes tarefas.
* **_Freelancers_:** Pretendem encontrar diferentes tarefas catalogadas.


#### Pré-condições
Existência de categorias de tarefas.

#### Pós-condições
A informação da Tarefa é especificada no sistema e disponibilizada aos freelancers.

### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia a definição de uma nova Tarefa.
2. O sistema solicita os dados necessários (i.e. referencia, designação, descrição informal, descrição de caracter técnico, estimativa de duração, estimativa de custo, categoria em que se enquadra).
3. O colaborador de organização introduz os dados solicitados.
4. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.
5. O colaborador de organização confirma.
6. O sistema regista os dados e informa o administrativo do sucesso da operação.

#### Extensões (ou fluxos alternativos)

a. O colaborador solicita o cancelamento da introdução da Tarefa.
> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>>	2a. O Colaborador não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o Colaborador para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>> 2a. O administrativo não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados obrigatórios para além dos já conhecidos?
* Todos os dados serão obrigatórios?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC6_MD.png](UC6_MD.png)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de organização inicia a definição de uma nova Tarefa. |	... interage com o utilizador? | EspecificarTarefaUI    |  Pure Fabrication |
|  		 |	... coordena o UC?	| EspecificarTarefaController | Controller    |
|  		 |	... cria instância de EspecificarTarefa? | Plataforma   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários (i.e. referencia, designação, descrição informal, descrição de caracter técnico, estimativa de duração, estimativa de custo, categoria em que se enquadra).  		 |							 |             |                              |
| 3. O administrativo introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   EspecificarTarefa | Information Expert (IE) - instância criada no passo 1     |
| 4. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.   		 |	...valida os dados da Tarefa (validação local) | EspecificarTarefa   |  IE. Possui os seu próprios dados.|
|	 |	...valida os dados da Tarefa (validação global) | Plataforma  | IE: A Plataforma possui/agrega EspecificarTarefa  |
| 5. O administrativo confirma.   		 |							 |             |                              |
| 6. O sistema regista os dados e informa o administrativo do sucesso da operação.  		 |	... guarda a EspecificarTarefa criada? | Plataforma  | IE: No MD a Plataforma possui EspecificarTarefa|


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * EspecificarTarefa


Outras classes de software (i.e. Pure Fabrication) identificadas:

 * EspecificarTarefaUI
 * EspecificarTarefaController


###	Diagrama de Sequência

![UC6_SD.png](UC6_SD.png)


###	Diagrama de Classes

![UC6_CD.png](UC6_CD.png)
