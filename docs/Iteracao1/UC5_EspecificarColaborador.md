# UC 5 - Especificar Colaboradores

# 1. Engenharia de Requisitos

### Formato Breve

O Gestor da Organização inicia a definição de especificar colaboradores da organização. O sistema solicita os dados de um colaborador (nome, função, contacto telefónico, endereço de email). O gestor introduz os dados dos colaboradores. O sistema valida os dados, apresenta-os e solicita confirmação. O gestor confirma e a operação termina.

### SSD
![UC5_SSD](UC5_SSD.png)


### Formato Completo

#### Ator principal

GESTOR DE ORGANIZAÇÃO

#### Partes interessadas e seus interesses
* **GESTOR DE ORGANIZAÇÃO:** pretende registar os colaboradores da sua empresa para que posteriormente estes possam aceder à plataforma.
* **COLABORADOR:** pretende aceder à plataforma.
* **T4J:** pretende que os gestores explicitem informações dos colaboradores para posteriormente estes terem acesso à plataforma.


#### Pré-condições
Registo da empresa e do respetivo Gestor de organização.

#### Pós-condições
Informações dos colaboradores são guardadas em sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O gestor inicia a especificação dos colaboradores.
2. O sistema solicita os dados sobre o colaborador (nome, função, contacto telefónico, endereço de email).
3. O gestor introduz os dados solicitados.
4. O sistema apresenta os dados pedindo confirmação.
5. O gestor confirma.
6. O sistema regista os dados dos colaboradores e informa o gestor do sucesso da operação.


#### Extensões (ou fluxos alternativos)

 ****a. O gestor de organização solicita o cancelamento da definição da  área de atividade.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O gestor de organização não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o gestor de organização para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O gestor de organização não altera os dados. O caso de uso termina.

4c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o gestor de organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O gestor de organização não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
 * Quantos colaboradores pode registar?
 * Os dados pedidos são suficientes?
 * Os dados pedidos estão em demasia?


# 2. Análise OO

## 1.Modelo de Domínio

![UC5.MD](UC5.MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1. O gestor inicia a especificação dos colaboradores.|... interage com o gestor?| EspecificarColaboradorUI |Pure Fabrication|
| |... coordena o UC?| EspecificarColaboradorController |Controller|
|2. O sistema solicita os dados sobre o colaborador (nome, função, contacto telefónico, endereço de email).||||
|3. O gestor introduz os dados solicitados.|... guarda os dados introduzidos?|Colaborador|IE: instância criada no passo 1|
| |... cria instâncias de Colaborador?|Organização|creator(regra1)|
|4. O sistema apresenta os dados pedindo confirmação. |... valida os dados do Colaborador (validação local)|Colaborador|IE: possui os seus próprios dados|
| |... valida os dados do Colaborador (validação global)|Organização|IE: A plataforma possui Colaborador|
| |... apresenta os dados de confirmação|EspecificarColaboradorUI||
|5. O gestor confirma.||||
|6. O sistema regista os dados dos colaboradores e informa o gestor do sucesso da operação.|... guarda o Colaborador criado?| Organização |IE: No MD a Plataforma possui Colaborador|
| |... regista/guarda o Utilizador referente ao Colaborador da Organização?|AutorizacaoFacade|IE. A gestão de utilizadores é responsabilidade do componente externo respetivo cujo ponto de interação é através da classe "AutorizacaoFacade"|



### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Classe1
 * Classe2
 * Classe3


Outras classes de software (i.e. Pure Fabrication) identificadas:  

* xxxxUI
* xxxxController


###	Diagrama de Sequência

![UC5_DS.png](UC5_DS.png)


###	Diagrama de Classes

![UC5_DC.png](UC5_DC.png)
