package pt.ipp.isep.dei.esoft.pot.model;

import java.util.List;

/**
 *
 * @author Tiago
 */
public class CategoriaTarefa {
    
    private String descriçao;
    private String areaAtividade;
    private List                 listaCompetencias;
                //<Competencias>
    
    public CategoriaTarefa (String descriçao, String areaAtividade, List listaCompetencias){
        // valida que os valores necessários
        if( (descriçao== null) || (areaAtividade== null) || (listaCompetencias== null) || (descriçao.isEmpty()) 
                || (areaAtividade.isEmpty()) || (listaCompetencias.isEmpty())){
            System.out.println("Nenhum dos argumentos pode ser nulo ou vazio.");
        } else {
        // guarda a informação 
        this.descriçao = descriçao;
        this.areaAtividade = areaAtividade;
        this.listaCompetencias = listaCompetencias;
        }
    }
    
}
