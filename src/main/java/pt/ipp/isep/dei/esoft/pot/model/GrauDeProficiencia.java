package pt.ipp.isep.dei.esoft.pot.model;

/**
 * The type Grau de proficiencia.
 */
public class GrauDeProficiencia {
    private int valor;
    private String designacao;

    /**
     * Instantiates a new Grau de proficiencia.
     *
     * @param valor      the valor
     * @param designacao the designacao
     */
    public GrauDeProficiencia (int valor, String designacao){
        this.valor = valor;
        this.designacao = designacao;
    }

    /**
     * Gets valor.
     *
     * @return the valor
     */
    public int getValor() {
        return valor;
    }

    /**
     * Get designacao string.
     *
     * @return the string
     */
    public String getDesignacao(){
        return designacao;
    }

    /**
     * Set valor.
     *
     * @param valor the valor
     */
    public void setValor(int valor){
        this.valor = valor;
    }

    /**
     * Set designacao.
     *
     * @param designacao the designacao
     */
    public void setDesignacao(String designacao){
        this.designacao=designacao;
    }

    @Override
    public String toString(){
        return String.format("Grau de Proficiência %d : %s", valor,designacao);
    }
}
