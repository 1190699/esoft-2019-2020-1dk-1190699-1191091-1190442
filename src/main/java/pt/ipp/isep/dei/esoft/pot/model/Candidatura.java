package pt.ipp.isep.dei.esoft.pot.model;

import javax.xml.crypto.Data;

/**
 * The type Candidatura.
 */
public class Candidatura {

    private Data dataCandidatura;
    private double valorPretendido;
    private int nrDias;
    private String textoApresentacao;
    private String textoMotivacao;

    /**
     * Gets data candidatura.
     *
     * @return the data candidatura
     */
    public Data getDataCandidatura() {
        return dataCandidatura;
    }

    /**
     * Sets data candidatura.
     *
     * @param dataCandidatura the data candidatura
     */
    public void setDataCandidatura(Data dataCandidatura) {
        this.dataCandidatura = dataCandidatura;
    }

    /**
     * Gets valor pretendido.
     *
     * @return the valor pretendido
     */
    public double getValorPretendido() {
        return valorPretendido;
    }

    /**
     * Sets valor pretendido.
     *
     * @param valorPretendido the valor pretendido
     */
    public void setValorPretendido(double valorPretendido) {
        this.valorPretendido = valorPretendido;
    }

    /**
     * Gets nr dias.
     *
     * @return the nr dias
     */
    public int getNrDias() {
        return nrDias;
    }

    /**
     * Sets nr dias.
     *
     * @param nrDias the nr dias
     */
    public void setNrDias(int nrDias) {
        this.nrDias = nrDias;
    }

    /**
     * Gets texto apresentacao.
     *
     * @return the texto apresentacao
     */
    public String getTextoApresentacao() {
        return textoApresentacao;
    }

    /**
     * Sets texto apresentacao.
     *
     * @param textoApresentacao the texto apresentacao
     */
    public void setTextoApresentacao(String textoApresentacao) {
        this.textoApresentacao = textoApresentacao;
    }

    /**
     * Gets texto motivacao.
     *
     * @return the texto motivacao
     */
    public String getTextoMotivacao() {
        return textoMotivacao;
    }

    /**
     * Sets texto motivacao.
     *
     * @param textoMotivacao the texto motivacao
     */
    public void setTextoMotivacao(String textoMotivacao) {
        this.textoMotivacao = textoMotivacao;
    }
}
}
