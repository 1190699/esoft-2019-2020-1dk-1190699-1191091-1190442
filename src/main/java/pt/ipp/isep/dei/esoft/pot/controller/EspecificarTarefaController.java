package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.CategoriaTarefa;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;

/**
 *
 * @author Tiago
 */
public class EspecificarTarefaController {
    
    private Plataforma plataforma;
    private CategoriaTarefa categoria;    
    
    public void novaCategoriaTarefa(String descriçao, String areaAtividade, List competencias) throws Exception{ 
        try {
            this.categoria = this.plataforma.novaCategoriaTarefa(descriçao, areaAtividade, competencias);
            if (!this.plataforma.validaCategoriaTarefa(categoria)){
                throw new Exception( "Categoria de Tarefa inválida");
            }
        }
        catch(Exception e) {
            throw e;
        }
    }
    
    public void registaCategoriaTarefa () throws Exception {
        try {
           this.plataforma.registaCategoriaTarefa(this.categoria);
        } catch (Exception e) {
            throw e;
        }
    }
    
}
