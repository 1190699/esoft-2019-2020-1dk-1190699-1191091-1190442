package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.pot.controller.AplicacaoPOT;

public class RegistoOrganizacao {
     
    private Set<Organizacao> m_lstOrganizacoes;
    private Plataforma m_oPlataforma;
    private AplicacaoPOT m_oApp; 
    private  AutorizacaoFacade m_oAutorizacao;
    
    public RegistoOrganizacao(){
        this.m_oApp = AplicacaoPOT.getInstance();
        this.m_oPlataforma = m_oApp.getPlataforma();        
        this.m_oAutorizacao = m_oPlataforma.getAutorizacaoFacade(); 
    }
    
    public Organizacao novaOrganizacao(String strNome, String strNIF, String strWebsite, String strTelefone, String strEmail, EnderecoPostal oMorada, Colaborador oGestor) {
        return new Organizacao(strNome, strNIF, strWebsite, strTelefone, strEmail, oMorada, oGestor);
    }

    public boolean registaOrganizacao(Organizacao oOrganizacao, String strPwd) {
        if (this.validaOrganizacao(oOrganizacao, strPwd)) {
            Colaborador oGestor = oOrganizacao.getGestor();
            String strNomeGestor = oGestor.getNome();
            String strEmailGestor = oGestor.getEmail();
            if (this.m_oAutorizacao.registaUtilizadorComPapeis(strNomeGestor, strEmailGestor, strPwd,
                    new String[]{Constantes.PAPEL_GESTOR_ORGANIZACAO, Constantes.PAPEL_COLABORADOR_ORGANIZACAO})) {
                return addOrganizacao(oOrganizacao);
            }
        }
        return false;
    }

    private boolean addOrganizacao(Organizacao oOrganizacao) {
        return m_lstOrganizacoes.add(oOrganizacao);
    }

    public boolean validaOrganizacao(Organizacao oOrganizacao, String strPwd) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        if (this.m_oAutorizacao.existeUtilizador(oOrganizacao.getGestor().getEmail())) {
            bRet = false;
        }
        if (strPwd == null) {
            bRet = false;
        }
        if (strPwd.isEmpty()) {
            bRet = false;
        }
        //

        return bRet;
    }
    
    public Organizacao getOrganizacaoByEmailUtilizador(String email){
        for(Organizacao cont : m_lstOrganizacoes){
            if(cont.getGestor().getEmail().equals(email)){
                return cont;
            }
        }
        return null;
    }
}
