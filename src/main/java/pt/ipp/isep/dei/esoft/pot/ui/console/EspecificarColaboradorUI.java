

package pt.ipp.isep.dei.esoft.pot.ui.console;

import pt.ipp.isep.dei.esoft.pot.controller.RegistarColaboradorController;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EspecificarColaboradorUI {
    private RegistarColaboradorController m_controller;
    
    
    public void run(){
        System.out.println("Registar Organização:");
        if(introduzDados()){
            
        }
    }
    public boolean introduzDados(){
        System.out.println("Informações sobre o Colaborador:");
        String nome = Utils.readLineFromConsole("Nome do Colaborador: ");
        String funcao = Utils.readLineFromConsole("Função desempenhada: ");
        String contactoTelefonico = Utils.readLineFromConsole("Contacto Telefónico: ");
        String email = Utils.readLineFromConsole("Email do Colaborador: ");
        String pwd = gerarPassword();
        m_controller.determinarOrganizacao();
        return m_controller.novoColaborador(nome,funcao,contactoTelefonico,email,pwd);
    }
    private void apresentaDados(){
        System.out.println("Informação a registar: ");
    }

    private String gerarPassword() {
        String pwd="colaborador";
        return pwd;
    }
}
