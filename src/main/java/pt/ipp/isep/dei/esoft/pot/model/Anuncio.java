package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;

/**
 * The type Anuncio.
 */
public class Anuncio {

    private Date InicioPeriodoPublicitacao;
    private Date FimPeriodoPublicitacao;
    private Date InicioPeriodoCandidaturaFreelancer;
    private Date FimPeriodoCandidaturaFreelancer;
    private Date InicioPeriodoSeriacaoEAtribuicao;
    private Date FimPeriodoSeriacaoEAtribuicao;

    /**
     * Gets inicio periodo publicitacao.
     *
     * @return the inicio periodo publicitacao
     */
    public Date getInicioPeriodoPublicitacao() {
        return InicioPeriodoPublicitacao;
    }

    /**
     * Sets inicio periodo publicitacao.
     *
     * @param inicioPeriodoPublicitacao the inicio periodo publicitacao
     */
    public void setInicioPeriodoPublicitacao(Date inicioPeriodoPublicitacao) {
        InicioPeriodoPublicitacao = inicioPeriodoPublicitacao;
    }

    /**
     * Gets fim periodo publicitacao.
     *
     * @return the fim periodo publicitacao
     */
    public Date getFimPeriodoPublicitacao() {
        return FimPeriodoPublicitacao;
    }

    /**
     * Sets fim periodo publicitacao.
     *
     * @param fimPeriodoPublicitacao the fim periodo publicitacao
     */
    public void setFimPeriodoPublicitacao(Date fimPeriodoPublicitacao) {
        FimPeriodoPublicitacao = fimPeriodoPublicitacao;
    }

    /**
     * Gets inicio periodo candidatura freelancer.
     *
     * @return the inicio periodo candidatura freelancer
     */
    public Date getInicioPeriodoCandidaturaFreelancer() {
        return InicioPeriodoCandidaturaFreelancer;
    }

    /**
     * Sets inicio periodo candidatura freelancer.
     *
     * @param inicioPeriodoCandidaturaFreelancer the inicio periodo candidatura freelancer
     */
    public void setInicioPeriodoCandidaturaFreelancer(Date inicioPeriodoCandidaturaFreelancer) {
        InicioPeriodoCandidaturaFreelancer = inicioPeriodoCandidaturaFreelancer;
    }

    /**
     * Gets fim periodo candidatura freelancer.
     *
     * @return the fim periodo candidatura freelancer
     */
    public Date getFimPeriodoCandidaturaFreelancer() {
        return FimPeriodoCandidaturaFreelancer;
    }

    /**
     * Sets fim periodo candidatura freelancer.
     *
     * @param fimPeriodoCandidaturaFreelancer the fim periodo candidatura freelancer
     */
    public void setFimPeriodoCandidaturaFreelancer(Date fimPeriodoCandidaturaFreelancer) {
        FimPeriodoCandidaturaFreelancer = fimPeriodoCandidaturaFreelancer;
    }

    /**
     * Gets inicio periodo seriacao e atribuicao.
     *
     * @return the inicio periodo seriacao e atribuicao
     */
    public Date getInicioPeriodoSeriacaoEAtribuicao() {
        return InicioPeriodoSeriacaoEAtribuicao;
    }

    /**
     * Sets inicio periodo seriacao e atribuicao.
     *
     * @param inicioPeriodoSeriacaoEAtribuicao the inicio periodo seriacao e atribuicao
     */
    public void setInicioPeriodoSeriacaoEAtribuicao(Date inicioPeriodoSeriacaoEAtribuicao) {
        InicioPeriodoSeriacaoEAtribuicao = inicioPeriodoSeriacaoEAtribuicao;
    }

    /**
     * Gets fim periodo seriacao e atribuicao.
     *
     * @return the fim periodo seriacao e atribuicao
     */
    public Date getFimPeriodoSeriacaoEAtribuicao() {
        return FimPeriodoSeriacaoEAtribuicao;
    }

    /**
     * Sets fim periodo seriacao e atribuicao.
     *
     * @param fimPeriodoSeriacaoEAtribuicao the fim periodo seriacao e atribuicao
     */
    public void setFimPeriodoSeriacaoEAtribuicao(Date fimPeriodoSeriacaoEAtribuicao) {
        FimPeriodoSeriacaoEAtribuicao = fimPeriodoSeriacaoEAtribuicao;
    }
    
    //uc12
    /**
     * 
     * @param aFr 
     */
    public getCandidaturaFreelancer(Object aFr) {
		throw new UnsupportedOperationException();
	}
    
    
	public elegivel(Object aCand) {
		
	}

}
