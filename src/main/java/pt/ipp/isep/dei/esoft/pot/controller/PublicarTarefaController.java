/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacao;

/**
 *
 * @author joaod
 */
public class PublicarTarefaController {
    
    private Organizacao org;
    private Plataforma m_oPlataforma;
    
    public void getOrganizacaoDaSessaoAtual(){
        AplicacaoPOT app = AplicacaoPOT.getInstance();
        SessaoUtilizador sessao = app.getSessaoAtual();
        String email = sessao.getEmailUtilizador();
        m_oPlataforma = app.getPlataforma();
        RegistoOrganizacao registo = m_oPlataforma.getRegistoOrganizacao();
        org = registo.getOrganizacaoByEmailUtilizador(email);
        
        
    }
    
}
