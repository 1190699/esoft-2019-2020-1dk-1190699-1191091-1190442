/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author paulomaio
 */
public class EspecificarCompetenciaTecnicaController
{
    private Plataforma m_oPlataforma;
    private CompetenciaTecnica competencia;
    public EspecificarCompetenciaTecnicaController()
    {
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
    
    public List<AreaAtividade> getAreasAtividade()
    {
        return this.m_oPlataforma.getAreasAtividade();
    }
    
    public boolean novaCompetenciaTecnica(String cod, String dsBreve, String dsDet, String areaAtividadeId)
    {
        try
        {
            AreaAtividade area = this.m_oPlataforma.getAreaAtividadeById(areaAtividadeId);
            this.competencia = this.m_oPlataforma.novaCompetenciaTecnica(cod, dsBreve,dsDet,area);
            return this.m_oPlataforma.validaCompetenciaTecnica(this.competencia);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.competencia = null;
            return false;
        }
    }
   
    
    public boolean registarCompetenciaTecnica()
    {
        return this.m_oPlataforma.registaCompetenciaTecnica(this.competencia);
    }

    public String getCompetenciaTecnicaString()
    {
        return this.competencia.toString();
    }
}
