/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Plataforma {

    private String m_strDesignacao;
    private final AutorizacaoFacade m_oAutorizacao;
    private final Set<Organizacao> m_lstOrganizacoes;
    private final Set<Colaborador> m_lstColaboradores;
    private final Set<AreaAtividade> m_lstAreasAtividade;
    private final Set<CompetenciaTecnica> m_lstCompetencias;
    private final Set<CategoriaTarefa> m_lstCategorias;
    private final RegistoOrganizacao m_rgstOrganizacao;

    public Plataforma(String strDesignacao) {
        if ((strDesignacao == null)
                || (strDesignacao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strDesignacao = strDesignacao;

        this.m_oAutorizacao = new AutorizacaoFacade();

        this.m_lstOrganizacoes = new HashSet<>();
        this.m_rgstOrganizacao = new RegistoOrganizacao();
        this.m_lstAreasAtividade = new HashSet<>();
        this.m_lstCompetencias = new HashSet<>();
        this.m_lstColaboradores = new HashSet<>();
        this.m_lstCategorias = new HashSet<>();    
    }

    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }

    // Organizações
    // <editor-fold defaultstate="collapsed">
   
    public RegistoOrganizacao getRegistoOrganizacao(){
        return this.m_rgstOrganizacao;
    }  
   

    // </editor-fold>
    // Competências Tecnicas
    // <editor-fold defaultstate="collapsed">
    public CompetenciaTecnica getCompetenciaTecnicaById(String strId) {
        for (CompetenciaTecnica oCompTecnica : this.m_lstCompetencias) {
            if (oCompTecnica.hasId(strId)) {
                return oCompTecnica;
            }
        }

        return null;
    }

    public CompetenciaTecnica novaCompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoCompleta, AreaAtividade oArea) {
        return new CompetenciaTecnica(strId, strDescricaoBreve, strDescricaoCompleta, oArea);
    }

    public boolean registaCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        if (this.validaCompetenciaTecnica(oCompTecnica)) {
            return addCompetenciaTecnica(oCompTecnica);
        }
        return false;
    }

    private boolean addCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        return m_lstCompetencias.add(oCompTecnica);
    }

    public boolean validaCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }
    // </editor-fold>

    // Areas de Atividade 
    // <editor-fold defaultstate="collapsed">
    public AreaAtividade getAreaAtividadeById(String strId) {
        for (AreaAtividade area : this.m_lstAreasAtividade) {
            if (area.hasId(strId)) {
                return area;
            }
        }

        return null;
    }

    public AreaAtividade novaAreaAtividade(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada) {
        return new AreaAtividade(strCodigo, strDescricaoBreve, strDescricaoDetalhada);
    }

    public boolean registaAreaAtividade(AreaAtividade oArea) {
        if (this.validaAreaAtividade(oArea)) {
            return addAreaAtividade(oArea);
        }
        return false;
    }

    private boolean addAreaAtividade(AreaAtividade oArea) {
        return m_lstAreasAtividade.add(oArea);
    }

    public boolean validaAreaAtividade(AreaAtividade oArea) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }

    public List<AreaAtividade> getAreasAtividade() {
        List<AreaAtividade> lc = new ArrayList<>();
        lc.addAll(this.m_lstAreasAtividade);
        return lc;
    }

    // </editor-fold>
    public Colaborador novoColaborador(String nome, String email, String telefone, String funcao) {

        return new Colaborador(nome, funcao, telefone, email);

    }

    public boolean validaColaborador(Colaborador colaborador) {

        boolean booleanReturn = true;

        // Escrever aqui o código de validação
        //
        return booleanReturn;

    }

    public boolean registaColaborador(Colaborador colaborador) {
        if (this.validaColaborador(colaborador)) {

            String stringNomeGestor = colaborador.getNome();
            String stringEmailGestor = colaborador.getEmail();
            if (this.m_oAutorizacao.registaUtilizadorComPapeis(stringNomeGestor, stringEmailGestor, gerarPassword(), new String[]{Constantes.PAPEL_COLABORADOR_ORGANIZACAO})) {
                return addColaborador(colaborador);
            }
        }
        return false;
    }

    private boolean addColaborador(Colaborador colaborador) {
        return m_lstColaboradores.add(colaborador);
    }

    public String gerarPassword() {
        return "passwordrandom";

        //falta implementar este metodo
    }

    //Categoria Tarefa
    public CategoriaTarefa novaCategoriaTarefa (String descriçao, String areaAtividade, List ListaCompetencias) throws Exception {
        try {
            return new CategoriaTarefa (descriçao, areaAtividade, ListaCompetencias);
        } catch (Exception e){
            throw e;
        }
    }
    
    public boolean validaCategoriaTarefa(CategoriaTarefa categoria){
        //regras
        return true;
    }
    
    public boolean registaCategoriaTarefa (CategoriaTarefa categoria) throws Exception{
        if (this.validaCategoriaTarefa(categoria)){
            
            return addCategoriaTarefa(categoria); 
        }
        return false;
    }
    
    public boolean addCategoriaTarefa(CategoriaTarefa categoria){
        return m_lstCategorias.add(categoria);
    }
    
    //Seriar Anuncio
    public void getRegistoSeriaçao() {
	}

	public void getUtilizadorByEmail(Object aEmail) {
	}

	public void getAnuncioByEmailUtilizador(Object aEmail) {
	}

	public void getRegistoCandidatura() {
	}

	public void getRegistoColaborador() {
	}
    
    //uc12
        public void getRegistoFreelancer() {
		throw new UnsupportedOperationException();
	}

	public void getRegistoAnuncio() {
		throw new UnsupportedOperationException();
	}

}