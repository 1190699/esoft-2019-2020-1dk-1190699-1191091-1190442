
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class RegistarColaboradorController {
    private AplicacaoPOT m_oApp;
    private Plataforma m_oPlataforma;
    private Organizacao org;
    private SessaoUtilizador sessao;
    private String pwd;
    private Colaborador colaborador;
    private String id;
    
    public RegistarColaboradorController(){
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_GESTOR_ORGANIZACAO)) throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oApp=AplicacaoPOT.getInstance();
        this.m_oPlataforma=m_oApp.getPlataforma();
    }
    public boolean novoColaborador(String nome, String funcao, String contactoTelefonico, String email, String pwd){
        try{
            colaborador=Organizacao.novoColaborador(nome, funcao, contactoTelefonico, email);
            this.pwd=pwd;
            return this.org.validaColaborador(colaborador);
        }
        catch(RuntimeException ex){
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE,null,ex);
            return false;
        }
    }
    public boolean registaColaborador(){
        return this.m_oPlataforma.registaColaborador(colaborador);
    }
    public void determinarOrganizacao(){
        this.sessao=m_oApp.getSessaoAtual();
        this.id=this.sessao.getIdUtilizador();
        this.org=this.m_oPlataforma.getOrganizacao(id);
    }
    public String getColaboradorSTR(){
        return colaborador.toString();
    }

    /**public String determinarOrganizacao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }**/
}
